# !/usr/bin/env bash

########## USING GIT ##########

projectDir=`git rev-parse --show-toplevel`

templateFile='.config/.gitCommitTemplate'

git config --local commit.template $projectDir/$templateFile
