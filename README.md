# React Starter Kit

Developper : [ Mehdi C. ];

Fullstack starter kit for react projects using express.

## Require

![PHP from Packagist](https://img.shields.io/badge/Node-11.6.0-green.svg)
![PHP from Packagist](https://img.shields.io/badge/Npm-6.9.0-green.svg)
![PHP from Packagist](https://img.shields.io/badge/Yarn-1.19.1-green.svg)

## Install

##### :warning: every commands are written to be launch from root folder :warning:

- install packages :

```
(cd server && yarn (or npm ) install) && (cd client && yarn (or npm ) install)
```

##### Want to use a git commit message template ? ( optional )

- setup commit template :

```
(cd server && yarn (or npm ) run template) OR (cd client && yarn (or npm ) run template)
```

it will setup for you a template for yours commits. Use `git commit` instead `git commit -m "..."` for committing changes.

## Running

- :warning: Server is running under port : `5000`
- :warning: Client is running under port : `3000`

Make sure they are available before running.

- running server :

```
(cd server && yarn (or npm ) run dev)
```

- running client in dev mode ( in new terminal window ) :

```
(cd client && yarn (or npm ) run dev)
```

happy hackin..^^
