const app = require("express")();
const cors = require("cors");
const port = process.env.API_PORT || 5000;

app.use(cors());

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
